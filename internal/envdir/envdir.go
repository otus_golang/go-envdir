package envdir

import (
	"bufio"
	"errors"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

type Executor struct {
	append    bool
	fileUsage bool
	envs      []string
}

func NewExecutor(append bool, fileUsage bool) *Executor {
	return &Executor{append: append, fileUsage: fileUsage}
}

func (e *Executor) Exec(envPath string, commandToExecute string) error {
	err := e.parseEnvs(envPath)

	if err != nil {
		return err
	}

	cmd := exec.Command(commandToExecute)

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin

	if e.append {
		cmd.Env = append(os.Environ(), e.envs...)
	} else {
		cmd.Env = e.envs
	}

	return cmd.Run()
}

func (e *Executor) parseEnvs(path string) error {
	fi, err := os.Stat(path)
	if err != nil {
		log.Fatal(err)
	}

	switch mode := fi.Mode(); {
	case mode.IsDir():
		return e.parseEnvsFromDir(path)
	case mode.IsRegular():
		if !e.fileUsage {
			return errors.New("not allowed to use files")
		}
		return e.parseEnvsFromFile(path)
	}

	return errors.New("wrong type of file, probably it's not regular")
}

func (e *Executor) parseEnvsFromFile(path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		e.envs = append(e.envs, scanner.Text())
	}
	return scanner.Err()
}

func (e *Executor) parseEnvsFromDir(path string) error {

	files, err := filepath.Glob(path + "/*")

	if err != nil {
		return err
	}

	for _, filePath := range files {
		file, err := os.Open(filePath)
		if err != nil {
			return err
		}

		content, err := ioutil.ReadAll(file)
		if err != nil {
			return err
		}

		env := filepath.Base(filePath) + "=" + string(content)

		e.envs = append(e.envs, env)
	}

	return nil
}
