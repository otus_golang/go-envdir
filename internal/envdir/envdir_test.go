package envdir

import (
	"bytes"
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

type testData struct {
	executor       *Executor
	envPath        string
	command        string
	expectedOutput string
	error          bool
}

var tests = []testData{
	{
		NewExecutor(false, true),
		"./testdata/.test.env",
		"env",
		"APP_ENV=TEST\nAPP_DEBUG=FALSE\n",
		false,
	},
	{
		NewExecutor(false, true),
		"./testdata/envs",
		"env",
		"APP_ENV=TEST\nAPP_LOG=FALSE\n",
		false,
	},
	{
		NewExecutor(false, false),
		"./testdata/.test.env",
		"env",
		"",
		true,
	},
}

func TestExec(t *testing.T) {
	for _, testData := range tests {

		if testData.error {
			err := testData.executor.Exec(testData.envPath, testData.command)
			assert.Error(t, err)
		} else {
			var err error
			actual := captureStdout(func() {
				err = testData.executor.Exec(testData.envPath, testData.command)
			})

			assert.Equal(t, testData.expectedOutput, actual)
			assert.NoError(t, err)
		}
	}
}

func captureStdout(f func()) string {
	old := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	f()

	w.Close()
	os.Stdout = old

	var buf bytes.Buffer
	_, _ = io.Copy(&buf, r)

	return buf.String()
}
