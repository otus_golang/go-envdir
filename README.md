# go-envdir

## Decription
Реализовать утилиту envdir на Go.

Эта утилита позволяет запускать программы получая переменные окружения из определенной директории.
Пример использования:
```
go-envdir /path/to/env/dir some_prog
```

## Usage
```
  go-envdir [path to env file] [command] [flags]
```

```
Flags:
  -a, --append       append to current environment variables
  -h, --help         help for go-envdir
  -f, --parseFiles   allow to parse files (default true)
```