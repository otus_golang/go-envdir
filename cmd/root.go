package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/otus_golang/go-envdir/internal/envdir"
)

func init() {
	rootCmd.PersistentFlags().BoolVarP(&appendEnvs, "append", "a", false, "append to current environment variables")
	rootCmd.PersistentFlags().BoolVarP(&parseFiles, "parseFiles", "f", true, "allow to parse files")
}

var appendEnvs bool
var parseFiles bool

const argsCount int = 2

var rootCmd = &cobra.Command{
	Use:   "go-envdir [path to env file] [command]",
	Short: "Go Envdir",
	Long:  `go-envdir allow you to execute programs with environment variables from files`,
	Args:  cobra.ExactArgs(argsCount),
	Run: func(cmd *cobra.Command, args []string) {
		path := args[0]
		command := args[1]

		executor := envdir.NewExecutor(appendEnvs, parseFiles)
		err := executor.Exec(path, command)

		if err != nil {
			log.Fatal(err)
		}
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
