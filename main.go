package main

import "gitlab.com/otus_golang/go-envdir/cmd"

func main() {
	cmd.Execute()
}
